/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.Session;
import org.zkoss.zk.ui.Sessions;
import org.zkoss.zk.ui.util.GenericForwardComposer;
import org.zkoss.zul.Label;
import org.zkoss.zul.Textbox;
import org.hibernate.Query;
//import org.hibernate.Session;
import util.NewHibernateUtil;
import java.util.*;
/**
 *
 * @author Adi Yustiawan
 */
public class LoginController extends GenericForwardComposer{
    private Textbox username, password;
    private Label message;

    public void onClick$login() {
        if (!username.getValue().trim().equals("") && !password.getValue().trim().equals("")
                && username.getValue().equals("admin") && password.getValue().equals("admin")) {
            //org.hibernate.Session sess = NewHibernateUtil.getSessionFactory().openSession();
            //Query query = sess.createQuery("select iduser,namauser from user where iduser=1");
            
            //List users = query.list();
            //System.out.println(users.get(0));
            //set session to browser
            org.zkoss.zk.ui.Session session = org.zkoss.zk.ui.Sessions.getCurrent();
            session.setAttribute("userCredential", "user");

            Executions.sendRedirect("home.zul");
        } else {
            message.setValue("Login Incorrect");
        }
    }

    public void onOK$username() {
        onClick$login();
    }

    public void onOK$password() {
        onClick$login();
    }
}
