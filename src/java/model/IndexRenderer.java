/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import org.zkoss.zul.Listcell;
import org.zkoss.zul.Listitem;
import org.zkoss.zul.ListitemRenderer;
/**
 *
 * @author Adi Yustiawan
 */
public class IndexRenderer implements ListitemRenderer{

    @Override
    public void render(Listitem lstm, Object t, int i) throws Exception {
        final User data = (User) t;

        Listcell cell = new Listcell(String.valueOf(data.getIduser()));
        cell.setParent(lstm);

        cell = new Listcell(data.getNamasuser());
        cell.setParent(lstm); //To change body of generated methods, choose Tools | Templates.
    }
    
}
