package model;
// Generated Sep 8, 2021 5:16:35 AM by Hibernate Tools 4.3.1



/**
 * Divisi generated by hbm2java
 */
public class Divisi  implements java.io.Serializable {


     private int iddivisi;
     private String namadivisi;

    public Divisi() {
    }

	
    public Divisi(int iddivisi) {
        this.iddivisi = iddivisi;
    }
    public Divisi(int iddivisi, String namadivisi) {
       this.iddivisi = iddivisi;
       this.namadivisi = namadivisi;
    }
   
    public int getIddivisi() {
        return this.iddivisi;
    }
    
    public void setIddivisi(int iddivisi) {
        this.iddivisi = iddivisi;
    }
    public String getNamadivisi() {
        return this.namadivisi;
    }
    
    public void setNamadivisi(String namadivisi) {
        this.namadivisi = namadivisi;
    }




}


